Verso - Modern Binary Program Analysis
===========================================

Build with Electron, React and [Panopticon](https://gitlab.com/p8n). You need
Node.js 8, npm and Rust 1.15 or higher installed. Then type the following:

## Development setup

```bash
npm i
npm run dev
```

## Release build

```bash
npm i
npm run dist
```
