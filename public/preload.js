window.Panopticon = require('panopticon-node');
window.argument = "";
window.isDev = require('electron-is-dev');

const electron = require('electron');

const panopArg = "--panopticon=";

for (let x of electron.remote.process.argv) {
  if (x.startsWith(panopArg)) {
    window.argument = x.slice(panopArg.length);
    break;
  }
}

if (window.argument === "") {
  const { dialog } = electron.remote;

  window.argument = dialog.showOpenDialog({ properties: ['openFile'] })[0];
}
